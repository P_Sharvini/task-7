#!/usr/bin/env python
# coding: utf-8

# In[113]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
from sklearn.cluster import KMeans


# In[114]:


data = pd.read_csv('Temperature.csv')


# In[115]:


data


# In[116]:


x = data.iloc[:,[4,5]]


# In[117]:


x


# In[136]:


plt.scatter(data['AverageTemperatureFahr'],data['AverageTemperatureUncertaintyFahr'])
plt.xlim(50,70)
plt.ylim(30,45)
plt.show


# In[137]:


kmeans = KMeans(3)


# In[138]:


kmeans.fit(x)


# In[139]:


identified_clusters = kmeans.fit_predict(x)
identified_clusters


# In[140]:


data_with_cluster = data.copy()
data_with_cluster['cluster'] = identified_clusters
data_with_cluster


# In[145]:


plt.scatter(data_with_cluster['AverageTemperatureFahr'],data_with_cluster['AverageTemperatureUncertaintyFahr'],c=data_with_cluster['cluster'],cmap='rainbow')
plt.xlim(50,70)
plt.ylim(30,45)
plt.show()

